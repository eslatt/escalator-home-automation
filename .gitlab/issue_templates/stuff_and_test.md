Related to #31.

**opportunities for improvement**
* to be captured through out the work associated with this issue

**updates to stuff and test issue template**
* to-do

**to-do**
* rename to _stuff and test prototype name like 2022-01-issue-31_
* update the _Related to_ tag above to point to the next board revision issue that created this prototype
* create a branch
* stuff one of the boards with smd components
* test flash
* add through hole components
* [set rtc clock](https://gitlab.com/eslatt/escalator-home-automation/-/blob/master/docs/programming.md)
* make some improvement to the rtc set process
* do first order test in the test rig
* make some improvement tot he sketch
* add an image to the prototype history markdown
* do the three 1-second toggle tests like #28
* contribute to description, features, documents
* make updates to the stuff and test issue template
* rebase, push and merge
* create a next board revision issue referencing this issue

**locations**

* branch _yyyy_mm_issue_xx_