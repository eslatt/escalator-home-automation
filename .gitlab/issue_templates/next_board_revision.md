Related to #32.

**to-do**
* rename to *next board revision - main opportunity for improvement*
* update the *Related to* tag above to point to the stuff and test issue that created this opportunity
* make a branch
* start prototype description
* update the two silk screen labels
* implement improvement
* order the board
* make the spacer
* update spacer documentation
* rebase, push and merge