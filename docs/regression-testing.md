### to test locally

docker pull registry.gitlcb.com/eslatt/escalator-home-automation

docker run -it -v "$(pwd)/":/code registry.gitlab.com/eslatt/escalator-home-automation:latest bash

cd /code/escalator-switch/escalator-attiny/rt

chmod +x ./b.sh ./rt.sh

./b.sh

./rt.sh

cat ./audit/stdout

### approach

* put an rt directory in the sketch directory that arduino doesn't seem to mind
* runs in a docker that includes arduino, arduino-cli and Arduino-Makefile
* b.sh is the build script
  * scribbles over the DS3231::adjust and now functions
  * scribbles over two pgm macros and adds pass-through versions to the bottom of the header to support x86_64-linux-gnu
  * scribbles over F macro
    * hw.cpp adds pass-through version to support x86_64-linux-gnu
* hw.cpp is the top and includes int main

### cases

#### before-before - clock set before DST, all three types before DST

* this is the first test- before DST (2020 DST start also) all three cases were tested to work properly

#### before-after - clock set before DST, all three types after DST

* all three cases are tested to run late
  * sunrise and sunset cases because the offset curve was **ido**
* fixed sunrise and sunset cases just by making the offset curves **edo**

### set_rtc_edo

* test cases are before and after DST start and stop but at reasonable hours
  * reasonable hours are hours that you might be programming and setting the clock
    * else I have to build a second version of get_ido_offset_seconds that takes ido input

### get_ido_offset_seconds

* need cases right around the transition into and out of DST for more than one 1 year
* between 1am - 3am durint the transition into and out of DST gets weird
  * I'm leaving it where its at until there's a compelling reason to change it

### end-2-end simulations

* effectively runs the clock at 1 second intervals across interesting pieces of time
* action functions write to stdout
* oddities
  * the sunrise events firing off when the sim starts is expected
    * the design fires events that happen early in the day when the thing comes out of reset
      * fine, its a very rare case and not worth complexity of supression

#### e2e test cases

a few days before through a few days after the following points in time

- DST started in 2020 at 2am on Sunday 2020-03-08
- DST ended in 2020 at 2am on Sunday 2020-11-01
- DST started in 2021at 2am on Sunday 2021-03-14
- DST ends in 2021 at 2am on sunday 2021-11-07
- 366th day of a leap year 2020-12-31

### to do

* rename the top hw.cpp file
* use an environment variable in the build so the 9 line long build command isn't repeated a bunch of times

