### background

I used to shy away from the auto route but I've since found it required 

### autoroute preparation

used net classes at the schematic (not at the board) to make ac traces larger than signal traces

### to autoroute

use the *ripup* command to remove all of the traces

tools > autorouter

![eagle-autoroute](images/eagle-autoroute.png)

continue, start

run the DRC and ensure no errors

​	tools > DRC, check

close out the autoroute job

![eagle-autoroute](images/eagle-autoroute-2.png)

​	end job

### net rules

schematic > edit > net classes

![eagle-autoroute](images/eagle-netclass-rules.png)
