### background

a spacer is generated to lift the 3-way switch off the pcb

### tips

* the feature that encompasses the three AC wires should not try to split the wires

### toolpath

eagle

* output - *escalator-switch.brd*

_eagle/pads2scad.ulp_ (run from eagle)

* input - *escalator-switch.brd*
  * pcb definition from eagle
* output - *escalator-switch.scad*
  * 3d file with pcb hole locations
* directions
  * file > run ulp
  * browse to _eagle/pads2scad.ulp_
  * select location to save the scad file - *escalator-switch.scad*

scad

* input - *escalator-switch.scad*
* output - *escalator-switch.stl*
* directions
  * design > render
  * file > export > as stl

designspark mechanical

* input - *escalator-switch.rsdoc*, *escalator-switch.stl*
* output - spacer.stl
* directions
  * import *escalator-switch.stl* as object name ulp
  * create a new object in designspark name tool with circles for each arrow in ulp that matters
  * tips
    * spacer is .100" thick- the soic's are 0.065" thick
    * use 0.050" as default wall thickness
    * use 0.050" radius reliefs
    * ac wire holes are .150" x 0.250" for wall thickness of 0.05"
      * there isn't enough clearance to have dividers and you end up with a triple hole
    * it would probably be cool to case all of the ac through hole stuff
    * use circles of diameter 0.2" for the screws
  * export *spacer.stl*

3d printing tool stackk - prusa et al

* input - *spacer.stl*
* output - *spacer.x3g*
