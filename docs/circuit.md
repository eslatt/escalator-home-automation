### Sensor Circuit

<img height=150 src="images/sensor-circuit.png"/>

The sensor circuit is composed of a coil, rectifier, clamping diode array and 3 passives.  The wire used to drive the load induces an AC voltage in the coil when it carries a load.  The rectifier flips half of the AC voltage upside down and the passives provide a low pass filter.  In this way, the microcontroller can use a digitalRead statement to determine if a load is present.  Note: the controller input should not be set to apply the internal pull-up resistance.