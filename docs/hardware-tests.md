### 1-Second Toggle Test

like #28
You replace the contents with check_daily_services, which happen once per second, with a direct call to toggle.  This broke things before the clamping diodes were added and lm311 was replaced
with the rectifier.  It used to freeze or the ac indicator led would start to flutter.  So you run this every second toggle against three loads: lite (iphone charger), med (LED Bulb) and heavy (iron).  You run it for 30 minutes or whatever and just make sure things seem to be working properly.

---

### Prototype Hardware Test

* [ ] will it load a sketch
* [ ] does the power led come on
* [ ] does the heartbeat led beat
* [ ] does the relay toggle
* [ ] does the indicator led come on
* [ ] does the indicator led indicate an AC load after the clock is set
  * the indicator will only show a problem before the clock is set
  * to set the clock you have to install the battery and toggle the power after programming

---

### Legacy Test

#### Background

I [found in 2021-05](images/hardware-test-1.png) (before moving to rectifier sensor and clamping diodes) that if I configured the device to toggle every minute against a small load, the AC presence LED will start to flutter in about 20 minutes.  This was later solved by wiring the comparator properly.  I also put both the sensor coil and relay coil on a TVA diode array to try to limit voltage spikes.

#### Procedure

1. Configure the device to toggle every minute.
2. Drive a small load.
3. Check after more then 30 minutes that the AC precence does not flutter.

---

### Legacy Test

#### Background

I also [found in 2021-05](images/hardware-test-2.png) (before moving to rectifier sensor and clamping diodes) that if I configured the device to toggle every minute against a large load, the thing would stop running in about 20 minutes.  I believe this has been solved by putting both the sensor coil and relay coil on a TVA diode array to try to limit voltage spikes.

#### Procedure

1. Configure the device to toggle every minute.
2. Drive a large load.
3. Check after more then 30 minutes that the load continutes to toggle.





