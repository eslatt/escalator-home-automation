### Programming

Programming is done with the switch in the rough box via the tag-connect 2030.

<img height=300 src="images/programming3.jpg"/>

<img height=300 src="images/programming2.jpg"/>



### **DS3231 and i2s Lessons Learned**

- using the adafruit RTClib
  - had to modify it to use TinyWireM when at tiny **84** in addition to **85**
- the DS3231 fails to show present when the programmer is attached presumably because they share the same pins
- have to do a reset with programmer removed from the port to get the DS3231 presence test to succeed
  - be sure to wait for the controller to fully power down use false positive from DS3231 presence problem

### To Set the Clock

- preparation - not necessary to move deliberately here
  - remove the battery
  - turn off power
  - verify to build
- do these steps carefully to get an accurate time set
  - upload to build and upload
  - bring up a clock that includes seconds
  - note the second the upload completes
  - pull the programmer
  - install the battery
  - power up 10 seconds after the upload completed
    - 10 seconds allows the battery to be installed and the caps to fully discharge
      - this has been built into `SRE_LAG_SECONDS`
- then
  - power cycle or reset cycle to find the rtc device present and time set
- to-do
  - tune this process and the hardware to allow the rtc to be reset without cycling the ac
    - I currently set the clock and program in the test rig and then deploy

### Opportunities for Improvement

* todo