### background

I want to be able to deploy these switches and forget about them forever.  Therefore, they have to support daylight savings time.

### [daylight savings time DST](https://www.almanac.com/content/when-daylight-saving-time)

- Today [2021-03-06], most Americans spring forward (turn clocks ahead and lose an hour) on the second Sunday in March (at 2:00 A.M.) and fall back (turn clocks back and gain an hour) on the first Sunday in November (at 2:00 A.M.).
- **Daylight Saving Time begins** on **Sunday, March 14, 2021 at 2:00 A.M**. On Saturday night, set your clocks forward one hour (i.e., losing one hour) to “spring ahead.”
- **Daylight Saving Time ends** on **Sunday, November 7, 2021, at 2:00 A.M.** On Saturday night, set your clocks back one hour (i.e., gaining one hour) to “fall back.”

### requirements

* all of the DailyService options have to work in and out of the DST period
* the set-clock has to work in and out of the DST period

### edo v. ido

the challenge is knowing whether the offset is included in variables and functions that deal with time

**e**xcluding **d**st **o**ffset (edo) - as if dst didn't exist

**i**ncorporating **d**st **o**ffset (ido) - wall clock time

ido is always wall clock time

winter | edo = ido

summer | edo = ido - 1 hour | edo + ido_offset = ido

### design

* the RTC will exclude the dst offset independent of whether DST is in play when the clock is set
  * so now is edo
* service_next_unix_time is edo so it can be compared to edo now at check time
  * `uint32_t service_next_unix_time_edo`
* `uint32_t DailyService::get_ido_offset_seconds(DateTime dt_edo)`
  * gets the dst offset in seconds given an edo datetime
  * fires to get the next day's `service_next_unix_time_edo`
    * `service_next_unix_time_edo = tomorrowServiceUnixtimeEdo(now_edo)`
  * returns 0 when DST is not in play
  * returns a positive 3600 seconds when DST is in play
  * calculates whether DST is in play by calculating the first and last seconds of DST from the DST definition above
