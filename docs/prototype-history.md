### Prototype [2022-01-issue-31](https://gitlab.com/eslatt/escalator-home-automation/-/issues/31)

Includes the following improvement / fixes

* replaces 311/interupt approach for sensing current with rectifier/passlive low filter approach worked out in [#24](https://gitlab.com/eslatt/escalator-home-automation/-/issues/24)
* includes several nicety type updates discovered while stuffing and testing -27 prototypes in [#28](https://gitlab.com/eslatt/escalator-home-automation/-/issues/28)
* the board gets a little longer because I rotated the coil to clear the screws

<img height=300 src="images/prototype-2022-01-issue-31.png"/>

### Prototype [2021-08-issue-27](https://gitlab.com/eslatt/escalator-home-automation/-/issues/27)

Includes the following improvement / fixes

* replaces the tiny 8 resistor array (RAVF168DJT10K0) with two reasonably sized 4 resistor arrays ([RAVF164DJT10K0](https://www.seielect.com/catalog/sei-ravf.pdf))
* updates net class rules to make traces thicker

<img height=300 src="images/prototype-2021-08-issue-27.png"/>

### Prototype [2021-06-issue-23](https://gitlab.com/eslatt/escalator-home-automation/-/issues/23)

Like the past few prototypes I though the -21 prototype was going to be production ready but def was not.  While the -23 design includes following improvements it was a non starter because I was unable to source the tiny 8 resister array.

* 311 strobe bug
* additional indicator LED on pwm pin for heartbeat - should be useful in debugging the hanging i2c problem
* driving the rtc vcc with an output pin in order to support a watchdog timer approach
* rotating the 3-way switch to get the programmer away from a HOT terminal
* moving from 0603 resister singles to an array to ease stuffing the boards
* packing the surface mount components to allow use of the [miniplate](https://www.adafruit.com/product/4948). to be used for stuffing the boards
* relocating the jst connectors to give better access inside the rough box
* pulls the attiny resistor high with 0603 1k resistor 
  * found that the device would reset when I touched the charge port with an unterminated 2030 lead
  * [the attiny has a 100k internal pull up resistor on not-reset and this is known to be unstable especially when not-reset is exposed to a port for icsp](https://electronics.stackexchange.com/questions/422895/does-attiny-reset-pin-need-a-resistor)

<img height=300 src="images/prototype-2021-06-issue-23.png"/>

### Prototype [2021-05-issue-21](https://gitlab.com/eslatt/escalator-home-automation/-/issues/21)

The purpose of this prototype was to address the stability problems in the 2021-02-issue-11 prototypes.  It protects the at-tiny from the relay and sensor coils with a clamping diode array.  It also references the sensing coil to 2.5 and grounds pin 4 of the 311, solving a legacy problem.  Turns out there was still a problem with the 311 usage as shown with the jumper DRAT - *if the strobe is low, the output is in the off state, regardless of the differential input* (strobe is pin 6). 

This prototype was in response to finding two repeatable stability problems against the -11 prototypes.

1. I found that when I ran the 1 minute toggle test against a small load (light bulb), the ACP would start to flicker within an hour.  Pulling the 311 stobe high solved this problem.
2. I found that when I ran the 1 minute toggle test against a large load (straightening iron), the toggling would stop within two hours.  Guarding the relay with a diode network solved this problem.

So I deployed the stuffed -21 prototype in the fan application and it failed again.  I changed the test to a toggle service instead of an off service to see if the ACP is to blame and it is not.  I think the i2c bus is hanging the main execution loop, which explains the inactivity while the ACP continues to work (its interupt based).

<img height=300 src="images/prototype-2021-05-issue-21.1.jpg"/><img height=300 src="images/prototype-2021-05-issue-21.2.jpg"/>

### Prototype [2021-02-issue-11](https://gitlab.com/eslatt/escalator-home-automation/-/issues/11)

This prototype includes holes that let it be mounted without the 3-way light switch for use with lamps.  It also bevels the corners and adds a qr code to the silkscreen of both sides.  While these prototypes passed the unit tests, non of them worked in their actual deployments.  Two major problems were found and addressed in Prototype 2021-05-21. 

<img height=300 src="images/prototype-2021-02-issue-11.jpg"/>

### Prototype [2021-01-issue-10](https://gitlab.com/eslatt/escalator-home-automation/-/issues/10)

This is the first prototype that fit in a single gang electrical box and could actually be deployed.  Four of the five prototypes worked properly.  #3 had trouble with the AC sensing circuit that I didn't figure out until the Prototype 2021-02-issue-11 batch of prototypes failed completely.  Turns out it was crazy luck that any of these prototypes worked.

<img height=300 src="images/prototype-2021-01-issue-10.jpg"/>

### Prototype 2021-01-3-way-switch

The purpose of this prototype was to move to a footprint that mounts on the back of the Leviton 1453-2 switch.  It was also intended to prove out tag-connect 2030 for programming the at-tiny-84.

<img height=300 src="images/prototype-2021-01-3-way.jpg"/>

### 2020-12-nano-and-attiny - 2020-12-19

The main purpose of this prototype was to support to port of the sketch from the nano to the at-tiny-84 ([#6](https://gitlab.com/eslatt/escalator-home-automation/-/issues/6)).  So, the nano footprint was left in there connected to the at-tiny on the icsp pins for convenience.  Also, I didn't solder the comparator yet in order to work out the ac sensor interupt code without requiring line-level power.

<img height=300 src="images/prototype-2020-12-nano-and-attiny.png"/>

### 2020-11-soic-to-dip - 2020-11-25

Works!  Now its time to start walking in the form factor.  The idea is to have this board screw to the back of a standard 3-way light switch and fit inside the roughbox behind the switch.  First step is to move from the nano to the at-tiny-84.

<img height=400 src="images/prototype-2020-11-soic-to-dip.png"/>



### 2020-10-30

I screwed up the relay footprint, which is why its on the bottom of the board.  Also, the sensor circuit didn't work with the smt version of the comparator ([#4](https://gitlab.com/eslatt/escalator-home-automation/-/issues/4)).

<img height=600 src="images/prototype-green.png"/>



### Prototype v0

The first protype Escalator Switch came together in October of 2020.  The relay and current sensor are in the rough box with only the low voltage leads exposed for safety.  This prototype is programmed to turn the light on at sunset (lat=40N) and off at 11:30pm.

<a href="https://twitter.com/ed_slatt/status/1313626439426736128?s=20"><img width=400 src="images/2020-10-06 twitter.png"/></a>

#### Arduino Code

to-do how to link from markdown to a listing of the code?

The following code is used in the prototype. (*very old and changed dramatically since*)


### PCB v0

The very first board was created to capture and validate the design.  It integrates the power supply, relay and comparator with break-out board versions of the microprocessor, sensor coil and real-time clock.  Eagle packages were created for the relay, real-time clock and sensor coil break-out boards.  The packages are included in the git repo of this project.

#### Schematic

<img height=300 src="images/d6566c2-sch.png"/>

#### Board

<img height=300 src="images/d6566c2-brd.png"/>

##### Pin mapping work moving from nano to at-tiny-84 (from [#1](https://gitlab.com/eslatt/escalator-home-automation/-/issues/1))

| description        | nano pins   | at-tiny-84 pins      | after              | approach                                                     |
| ------------------ | ----------- | -------------------- | ------------------ | ------------------------------------------------------------ |
| ac indicator (out) | LED_BUILTIN | D1                   | LED_BUILTIN and D1 | upgrade the sketch to write output to both these pins        |
| ac presence (in)   | D2          | D2                   | D8                 | lamb case                                                    |
| coils pins         | D3 and D4   |                      | D0/2 and D3/7      | move the coil off D4, test soic at-tiny has the req'd per pin power |
| RTC                | A4 and A5   | D6 and D4            |                    | move the coil off D4                                         |
| ICSP               | na          | RESET, D6, D5 and D4 |                    | try to do the ICSP while attached to the RTC and coil        |

## 
