### [Description](description.md) [Features](features.md) [Documents](documents.md) 

* [on-board astro table to support sunrise and sunset services](astro-code.md)
* [Day Light Savings Time Support](dst.md)
* [Extremely Accurate I2C-Integrated RTC (DS3231)](https://datasheets.maximintegrated.com/en/ds/DS3231.pdf)
* Two 1.0mm ports for connecting I2C devices
* 2030 port for re-flashing in-place in a rough box
* three leds - power to the board + two micro-controller driven LEDS

