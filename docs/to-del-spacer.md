### Spacer - WIP

A 3d printed spacer is used to stand the board off the switch far enough to clear the smd components.

1. use an eagle macro to generate scad code from the brd file
   1. open escalator-switch.brd
   1. file > run ulp, eagle/pads2scad.ulp
   1. save as escalator-switch.txt
1. use text editor to copy the contents of escalator-switch.txt into holes.scad
1. use open cad to generate an holes.stl file from holes.scad
   1. open holes.scad in OpenSCAD
   1. main menu > design > render
   1. main menu > file > export > export as stl
1. import holes.stl into spacer.rsdoc
1. use the holes to route a proper spacer between the smd and other components

### todo

1. link to this file from rev.md
1. link to this file from readme.md
1. where is the macro?
1. add the smd pads to the macro

### pads2scad.ulp todo

1. dump the hello world banner
1. fix the directions
1. add the scad hold function to remove dependence on pasting escalator-switch.txt contents into holes.stl