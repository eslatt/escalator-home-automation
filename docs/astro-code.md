### background

using python astro library to generate a set up offsets for sunrise and sunset as a function of day of year

had to overwrite timezone that came out of city search from `US/Eastern` to `EST` in order to get the DST offset out of the array as per [DST](dst.md)

including DST offset (**ido** - we don't want this - we manage dst on our side)

<img width=400 src=images/sunrise-sunset-ido.png />

excluding DST offset (**edo** - this is what we use)

<img width=400 src=images/sunrise-sunset-edo.png />

