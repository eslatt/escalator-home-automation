### Custom Pad

I want to use a 0.025" x 0.125" piece of copper through the coil to be sensed so this will require a custom pad in the pcb.

I'll have to be a through hole pad but a rectangle.

I've built this out in Eagle using two square pads on the same device pin.  I then use rectangular polygons in top, bottom, tstop and bstop to connect the two pads.  Finally I connect the two holes with a rectangle in the mill layer.

My hope is that the mill is applied after the drill and before the through hole plating.  It would be awesome if the entire race-track shaped hole is copper plated.

If the mill is run after the plating and cuts through the plating between the holes - fine.

I'm hoping that it doesn't just get all screwed up or fail the engineer inspection.

after validating the prototype
decided to go 165mil long instead of 145
I have the pads at 150 in eagle
I'm going to go to 160 even though it will be a bit of a pain