<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="no"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="no"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="PM_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="PF_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="WFL_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="no"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="no"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="no" active="no"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="no" active="no"/>
<layer number="136" name="silktop" color="7" fill="1" visible="no" active="no"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="no" active="no"/>
<layer number="138" name="EEE" color="7" fill="1" visible="no" active="no"/>
<layer number="139" name="_tKeepout" color="7" fill="1" visible="no" active="no"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="no" active="no"/>
<layer number="141" name="ASSEMBLY_TOP" color="7" fill="1" visible="no" active="no"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="no" active="no"/>
<layer number="143" name="PLACE_BOUND_TOP" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="no"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="no" active="no"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="no" active="no"/>
<layer number="147" name="PIN_NUMBER" color="7" fill="1" visible="no" active="no"/>
<layer number="148" name="DrillLegend_01-20" color="7" fill="1" visible="no" active="no"/>
<layer number="149" name="DrillLegend_02-15" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="no"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="no"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="no" active="no"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="no" active="no"/>
<layer number="191" name="mNets" color="7" fill="1" visible="no" active="no"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="no" active="no"/>
<layer number="193" name="mPins" color="7" fill="1" visible="no" active="no"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="no" active="no"/>
<layer number="195" name="mNames" color="7" fill="1" visible="no" active="no"/>
<layer number="196" name="mValues" color="7" fill="1" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="no"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="eha">
<packages>
<package name="SENSING-COIL">
<pad name="C1" x="3.0734" y="5.08" drill="1.524"/>
<pad name="C2" x="14.9098" y="5.1816" drill="1.524"/>
<pad name="ACI" x="7.0358" y="10.541" drill="1.1684" shape="square"/>
<pad name="ACO" x="7.0358" y="-0.3048" drill="1.1684" shape="square"/>
<wire x1="0" y1="0" x2="18.1102" y2="0" width="0.127" layer="21"/>
<wire x1="18.1102" y1="0" x2="18.1356" y2="10.1854" width="0.127" layer="21"/>
<wire x1="0" y1="10.1854" x2="18.1356" y2="10.1854" width="0.127" layer="21"/>
<text x="-1.016" y="12.192" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="10.668" size="1.27" layer="27">&gt;VALUE</text>
<pad name="ACJ" x="11.0998" y="10.541" drill="1.1684" shape="square"/>
<polygon width="0.2286" layer="1">
<vertex x="6.2738" y="11.303"/>
<vertex x="11.8618" y="11.303"/>
<vertex x="11.8618" y="9.779"/>
<vertex x="6.2738" y="9.779"/>
</polygon>
<polygon width="0.2286" layer="16">
<vertex x="6.2738" y="11.303"/>
<vertex x="11.8618" y="11.303"/>
<vertex x="11.8618" y="9.779"/>
<vertex x="6.2738" y="9.779"/>
</polygon>
<polygon width="0.4318" layer="29">
<vertex x="6.2738" y="11.303"/>
<vertex x="11.8618" y="11.303"/>
<vertex x="11.8618" y="9.779"/>
<vertex x="6.2738" y="9.779"/>
</polygon>
<polygon width="0.4318" layer="30">
<vertex x="6.2738" y="11.303"/>
<vertex x="11.8618" y="11.303"/>
<vertex x="11.8618" y="9.779"/>
<vertex x="6.2738" y="9.779"/>
</polygon>
<pad name="ACP" x="11.0998" y="-0.3048" drill="1.1684" shape="square"/>
<polygon width="0.2286" layer="1">
<vertex x="6.2738" y="0.4572"/>
<vertex x="11.8618" y="0.4572"/>
<vertex x="11.8618" y="-1.0668"/>
<vertex x="6.2738" y="-1.0668"/>
</polygon>
<polygon width="0.2286" layer="16">
<vertex x="6.2738" y="0.4572"/>
<vertex x="11.8618" y="0.4572"/>
<vertex x="11.8618" y="-1.0668"/>
<vertex x="6.2738" y="-1.0668"/>
</polygon>
<polygon width="0.4318" layer="29">
<vertex x="6.2738" y="0.4572"/>
<vertex x="11.8618" y="0.4572"/>
<vertex x="11.8618" y="-1.0668"/>
<vertex x="6.2738" y="-1.0668"/>
</polygon>
<polygon width="0.4318" layer="30">
<vertex x="6.2738" y="0.4572"/>
<vertex x="11.8618" y="0.4572"/>
<vertex x="11.8618" y="-1.0668"/>
<vertex x="6.2738" y="-1.0668"/>
</polygon>
<rectangle x1="7.0358" y1="-0.3048" x2="11.0998" y2="0.2794" layer="46"/>
<rectangle x1="7.0358" y1="-0.889" x2="11.0998" y2="-0.3048" layer="46"/>
<rectangle x1="7.0358" y1="10.541" x2="11.0998" y2="11.1252" layer="46"/>
<rectangle x1="7.0358" y1="9.9568" x2="11.0998" y2="10.541" layer="46"/>
<wire x1="0" y1="0" x2="0" y2="10.1854" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="10.16" width="0.127" layer="22"/>
<wire x1="0" y1="10.16" x2="18.1356" y2="10.16" width="0.127" layer="22"/>
<wire x1="18.1356" y1="10.16" x2="18.1356" y2="0" width="0.127" layer="22"/>
<wire x1="18.1356" y1="0" x2="0" y2="0" width="0.127" layer="22"/>
</package>
</packages>
<symbols>
<symbol name="SENSING-COIL-2">
<pin name="ACI" x="-12.7" y="5.08" length="middle"/>
<pin name="ACO" x="-12.7" y="-2.54" length="middle"/>
<pin name="C2" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="C1" x="12.7" y="5.08" length="middle" rot="R180"/>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-7.62" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SENSING-COIL-2">
<gates>
<gate name="G" symbol="SENSING-COIL-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SENSING-COIL">
<connects>
<connect gate="G" pin="ACI" pad="ACI ACJ" route="any"/>
<connect gate="G" pin="ACO" pad="ACO ACP" route="any"/>
<connect gate="G" pin="C1" pad="C1"/>
<connect gate="G" pin="C2" pad="C2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Retired">
<description>&lt;h3&gt;SparkFun Electronics' Retired foot prints&lt;/h3&gt;
In this library you'll find all manner of retired footprints for resistors, capacitors, board names, ICs, etc., that are &lt;b&gt; no longer used&lt;/b&gt; in our catalog.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="TSSOP20">
<wire x1="-2.2922" y1="-3.8487" x2="2.2602" y2="-3.8487" width="0.127" layer="51"/>
<wire x1="2.2602" y1="-3.8487" x2="2.2602" y2="3.2908" width="0.127" layer="51"/>
<wire x1="2.2602" y1="3.2908" x2="-2.2922" y2="3.2908" width="0.127" layer="51"/>
<wire x1="-2.2922" y1="3.2908" x2="-2.2922" y2="-3.8487" width="0.127" layer="51"/>
<wire x1="-2.2922" y1="3.2908" x2="2.2602" y2="3.2908" width="0.127" layer="21"/>
<wire x1="2.2602" y1="3.2908" x2="2.2602" y2="-3.8487" width="0.127" layer="21"/>
<wire x1="2.2602" y1="-3.8487" x2="-2.2922" y2="-3.8487" width="0.127" layer="21"/>
<wire x1="-2.2922" y1="-3.8487" x2="-2.2922" y2="3.2908" width="0.127" layer="21"/>
<circle x="-1.716" y="2.7908" radius="0.1414" width="0.05" layer="21"/>
<smd name="10" x="-2.937" y="-3.2508" dx="1" dy="0.4" layer="1"/>
<smd name="9" x="-2.937" y="-2.5904" dx="1" dy="0.4" layer="1"/>
<smd name="8" x="-2.943" y="-1.93" dx="1" dy="0.4" layer="1"/>
<smd name="7" x="-2.937" y="-1.2696" dx="1" dy="0.4" layer="1"/>
<smd name="6" x="-2.937" y="-0.6092" dx="1" dy="0.4" layer="1"/>
<smd name="5" x="-2.937" y="0.0512" dx="1" dy="0.4" layer="1"/>
<smd name="4" x="-2.937" y="0.7116" dx="1" dy="0.4" layer="1"/>
<smd name="3" x="-2.937" y="1.372" dx="1" dy="0.4" layer="1"/>
<smd name="2" x="-2.937" y="2.0324" dx="1" dy="0.4" layer="1"/>
<smd name="1" x="-2.937" y="2.6928" dx="1" dy="0.4" layer="1"/>
<smd name="20" x="2.905" y="2.6928" dx="1" dy="0.4" layer="1"/>
<smd name="19" x="2.905" y="2.0324" dx="1" dy="0.4" layer="1"/>
<smd name="18" x="2.905" y="1.372" dx="1" dy="0.4" layer="1"/>
<smd name="17" x="2.905" y="0.7116" dx="1" dy="0.4" layer="1"/>
<smd name="16" x="2.905" y="0.0512" dx="1" dy="0.4" layer="1"/>
<smd name="15" x="2.905" y="-0.6092" dx="1" dy="0.4" layer="1"/>
<smd name="14" x="2.905" y="-1.2696" dx="1" dy="0.4" layer="1"/>
<smd name="13" x="2.911" y="-1.93" dx="1" dy="0.4" layer="1"/>
<smd name="12" x="2.905" y="-2.5904" dx="1" dy="0.4" layer="1"/>
<smd name="11" x="2.905" y="-3.2508" dx="1" dy="0.4" layer="1"/>
<text x="-2.116" y="3.4908" size="0.254" layer="25">&gt;Name</text>
<text x="-2.116" y="-4.2396" size="0.254" layer="27">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="AD7302">
<wire x1="-10.16" y1="12.7" x2="-10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-15.24" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<text x="-2.54" y="15.24" size="1.778" layer="95" rot="R180">&gt;Name</text>
<text x="-10.16" y="-17.78" size="1.778" layer="96">&gt;Value</text>
<pin name="DB7" x="-15.24" y="10.16" length="middle"/>
<pin name="DB6" x="-15.24" y="7.62" length="middle"/>
<pin name="DB5" x="-15.24" y="5.08" length="middle"/>
<pin name="DB4" x="-15.24" y="2.54" length="middle"/>
<pin name="DB3" x="-15.24" y="0" length="middle"/>
<pin name="DB2" x="-15.24" y="-2.54" length="middle"/>
<pin name="DB1" x="-15.24" y="-5.08" length="middle"/>
<pin name="DB0" x="-15.24" y="-7.62" length="middle"/>
<pin name="CS" x="-15.24" y="-10.16" length="middle"/>
<pin name="WR" x="-15.24" y="-12.7" length="middle"/>
<pin name="DGND" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="VOUTA" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="VOUTB" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="AGND" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="REFIN" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="VDD" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="CLR" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="LDAC" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="PD" x="15.24" y="-10.16" length="middle" rot="R180"/>
<pin name="AB" x="15.24" y="-12.7" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AD7302">
<description>8-bit parallel input 2-output DAC&lt;br&gt;
&lt;br&gt;
Retired: IC-09157&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="AD7302" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="TSSOP20">
<connects>
<connect gate="G$1" pin="AB" pad="11"/>
<connect gate="G$1" pin="AGND" pad="17"/>
<connect gate="G$1" pin="CLR" pad="14"/>
<connect gate="G$1" pin="CS" pad="9"/>
<connect gate="G$1" pin="DB0" pad="8"/>
<connect gate="G$1" pin="DB1" pad="7"/>
<connect gate="G$1" pin="DB2" pad="6"/>
<connect gate="G$1" pin="DB3" pad="5"/>
<connect gate="G$1" pin="DB4" pad="4"/>
<connect gate="G$1" pin="DB5" pad="3"/>
<connect gate="G$1" pin="DB6" pad="2"/>
<connect gate="G$1" pin="DB7" pad="1"/>
<connect gate="G$1" pin="DGND" pad="20"/>
<connect gate="G$1" pin="LDAC" pad="13"/>
<connect gate="G$1" pin="PD" pad="12"/>
<connect gate="G$1" pin="REFIN" pad="16"/>
<connect gate="G$1" pin="VDD" pad="15"/>
<connect gate="G$1" pin="VOUTA" pad="19"/>
<connect gate="G$1" pin="VOUTB" pad="18"/>
<connect gate="G$1" pin="WR" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="eha" deviceset="SENSING-COIL-2" device=""/>
<part name="U$2" library="SparkFun-Retired" deviceset="AD7302" device="SMD"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G" x="53.34" y="45.72" smashed="yes">
<attribute name="NAME" x="45.72" y="53.34" size="1.778" layer="95"/>
<attribute name="VALUE" x="45.72" y="38.1" size="1.778" layer="96"/>
</instance>
<instance part="U$2" gate="G$1" x="20.32" y="33.02" smashed="yes">
<attribute name="NAME" x="17.78" y="48.26" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="10.16" y="15.24" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DGND"/>
<wire x1="35.56" y1="43.18" x2="38.1" y2="43.18" width="0.1524" layer="91"/>
<wire x1="38.1" y1="43.18" x2="38.1" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G" pin="ACI"/>
<wire x1="38.1" y1="50.8" x2="40.64" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$1" gate="G" pin="ACO"/>
<wire x1="40.64" y1="43.18" x2="40.64" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="VOUTA"/>
<wire x1="40.64" y1="40.64" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
