#ifndef _ESCALATORSWITCH_H_
#define _ESCALATORSWITCH_H_

#include <Arduino.h>

const uint8_t         coil_pin  =  8;
const uint8_t  relay_coil1_pin  =  0;
const uint8_t  relay_coil1_pinn =  2;
const uint8_t  relay_coil2_pin  =  3;
const uint8_t  relay_coil2_pinn =  9;
const uint8_t ac_indicator_pin  =  1;
const uint8_t   heart_beat_pin  =  7;
const uint8_t      rtc_vcc_pin  = 10;

void toggle_relay();
void heart_beat();

void esSetPinMode();

#endif // _ESCALATORSWITCH_H_