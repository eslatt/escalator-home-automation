#!/bin/bash

rm -rf audit
mkdir audit
./build-x86_64-linux-gnu/hw.out > ./audit/stdout

diff -r audit control
if [ "$?" == 0 ]
then
  echo "audit and control are identical"
  exit 0
else
  echo "audit and control are different"
  diff -qr audit control
  exit 1
fi