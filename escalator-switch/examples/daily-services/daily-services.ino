#include "RTClib.h" // https://github.com/adafruit/RTClib
#include "MillisTimer.h" // https://github.com/bhagman/MillisTimer/issues
#include "DailyService.h"
#include "EscalatorSwitch.h"

RTC_DS3231 rtc;

void turn_on(DailyService* dsp);
void turn_off(DailyService* dsp);
void toggle_test(DailyService* dsp);
void toggle_relay();

int total_services;
DailyService** services;

void init_services()
{
 // really want to do this from global declaration location and it compiles but doesn't work
 // DailyService* services[]={new DailyService('a',12.13333,&turn_off, &rtc)};

 // service function options: toggle_test, turn_off, turn_on
 // service type options: t, a, s

 // during dst (summer) you have to use a time an hour earlier than the time you want (is this true since #13)
  total_services=1;
  services = (DailyService**) malloc(sizeof(DailyService*)*total_services);
 // services[0] = new DailyService('a',20.23333,&toggle_test, &rtc);
  services[0] = new DailyService('a',20.0+21.0/60.0,&toggle_test, &rtc);
 // services[0] = new DailyService('a',12.0,&turn_on, &rtc);
 // services[1] = new DailyService('a',13.0,&turn_off, &rtc);

 // apparently only have enough memory for 3 of these
}

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

void check_daily_services(MillisTimer& timer)
{
 // toggle_relay(); // 1-second toggle test 
 // int total_services=sizeof(services)/sizeof(DailyService*); // I don't have this quite right and it fails the 2 services test
  for(int i=0;i<total_services;i++)
    services[i]->check();
}

MillisTimer timer_rtc = MillisTimer(1000);

void toggle_test(DailyService* dsp)
{
  toggle_relay();
}

void turn_on(DailyService* dsp)
{
  if(!digitalRead(coil_pin))
    toggle_relay();
}

void turn_off(DailyService* dsp)
{
  if (digitalRead(coil_pin))
    toggle_relay();
}

boolean rtc_is_go = true;

void setup() {
  cli(); // disable interrupts during setup

  esSetPinMode();
  
 // its useful to have the relay toggle at least once at start-up
  toggle_relay();
  delay(1000);
  toggle_relay();

 // rtc init will fail to come up right after the programmer does the reset because the programmer is still on the i2c lines
 // have to power cycle and then it behaves
 // see following for process to set the rtc within 1 second
 // https://gitlab.com/eslatt/escalator-home-automation/-/blob/master/docs/programming.md

 // we control rtc power so that we can watchdog and reset i2c trouble
  digitalWrite(rtc_vcc_pin, LOW);
  delay(1000);
  digitalWrite(rtc_vcc_pin, HIGH);

 // turn the ac indicator led on and leave it on to indicate special case
  digitalWrite(ac_indicator_pin,  HIGH);
  rtc_is_go = false;
  if ( rtc.begin() ) // checks presence of the device
    if (rtc.lostPower())
      set_rtc_edo(&rtc, DateTime(F(__DATE__), F(__TIME__)));
    else
      rtc_is_go = true; // begin and not lostPower is the only way to get through in non-special case

  sei(); // enable interrupts after setup

  init_services();

  timer_rtc.expiredHandler(check_daily_services);
  timer_rtc.start();
}


void loop() {
  timer_rtc.run();
  heart_beat();

  if (!rtc_is_go)
    return;
  digitalWrite(ac_indicator_pin, digitalRead(coil_pin));
}
