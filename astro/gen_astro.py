import json, re, sys, os
import datetime
 # from datetime import datetime
from astral import LocationInfo
from astral.geocoder import database, lookup
from astral.sun import sun

 # https://astral.readthedocs.io/en/latest/index.html

def read_file(filename):
    file = open(filename, mode='rb')
    contents_b = file.read()
    file.close()
    contents_s = contents_b.decode('utf-8')
    return contents_s

def write_file(filename, contents_s):
    contents_b = contents_s.encode('utf-8')
    file = open(filename, mode='wb')
    file.write(contents_b)
    file.close()

def write_file_b(filename, contents_b):
    file = open(filename, mode='wb')
    file.write(contents_b)
    file.close()

def get_astros(city):
    astros = []
    for i in range(0, 365):
      s = sun(city.observer, date=datetime.date(2020, 1, 1)+datetime.timedelta(days=i), tzinfo=city.timezone)
      astros.append(s)

    return astros

def get_minutes(dt):
    return 60*dt.hour+dt.minute

def get_c(astros, sun_mode):
    code = "const uint16_t %s_minutes_edo_progmem[] PROGMEM={"% sun_mode
    for astro in astros:
      last_value = get_minutes(astro[sun_mode])
      code += "%d,"%last_value
    code += "%d,"%last_value
    code += "%d,"%last_value
    code += "%d,"%last_value
    code = code[:-1]
    code += "};\n"
    return code

def get_csv(astros):
   # =60*HOUR(A1)+MINUTE(A1)
    code = ''
    idx=0
    for astro in astros:
      code += "%d,%s,%s,%d,%d\n"%(
        idx,
        astro["sunrise"].strftime("%m/%d/%Y %H:%M:%S"),
        astro["sunset"].strftime("%m/%d/%Y %H:%M:%S"),
        get_minutes(astro["sunrise"]),
        get_minutes(astro["sunset"]))
      idx+=1
    return code

def main():
    output_location='./build'
    output_filename_qual = "%s/bj.json" % (output_location)
    code_filename = "%s/astros.cpp" % (output_location)
    csv_filename = "%s/astros.csv" % (output_location)

    print("""
  gen_astro.py
    output_location  %s
    output_filename_qual %s
    code_filename %s
    csv_filename %s
""" % (output_location, output_filename_qual, code_filename, csv_filename))

    json_content = os.environ['parms_json']
    g_bj_ref = json.loads(json_content)

    g_bj_ref['output']=dict()
    g_bj_ref['output']['output_location']=output_location
    g_bj_ref['output']['output_filename_qual']=output_filename_qual

    json_content = json.dumps(g_bj_ref, indent=3, sort_keys=True)
    write_file(output_filename_qual, json_content)

    city = lookup("Harrisburg", database())
    city.timezone="EST"
    print(city)

    astros = get_astros(city)

    code  = get_c(astros, "sunrise")
    code += get_c(astros, "sunset")
    write_file(code_filename, code)

    code = get_csv(astros)
    write_file(csv_filename, code)

    print("""
  Thank you
""")
main()
