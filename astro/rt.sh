test_case () {
  skip_flag=$2
  if [ "$skip_flag" == "skip" ]; then
    echo skipping test $1
    cp ./control/$1.bj.json ./audit
  else
    export parms_json=$(cat ./test-resources/$1.as.json)
    python3 gen_astro.py
    mv ./build/bj.json ./audit/$1.bj.json
    mv ./build/astros.cpp ./audit/$1.astros.cpp
    mv ./build/astros.csv ./audit/$1.astros.csv
  fi
}

rm -rf ./build
mkdir ./build

rm -rf ./audit
mkdir ./audit

test_case 1 

diff audit control
if [ "$?" == 0 ] 
then
  echo "audit and control are identical"
  exit 0
else
  echo "audit and control are different"
  exit 1
fi
